# IMMJ Technical Tests

This repository contains a set of technical tests to be completed as part of an IMMJ software
engineer recruitment process.

## Prerequisites

You will need [NodeJS](https://nodejs.org) >= 12.X - we recommend you use the latest **stable**
release.

## Instructions

This repository contains four tests that are available to be completed:

- Front-End Vanilla Javascript
- Front-End React
- Back-End Express
- Back-End GraphQL

You can complete any (or all) of the available tests, spending as much time as you see fit. We
don't expect you to spend hours and hours getting everything just right, just spend as much time as
you are comfortable with to give us a good demonstration of what you can do.

Fork this repository into a publically available repository, complete the test(s) as you see fit,
and when you are finished send us the link so we can take a look at your work.

Good luck!
