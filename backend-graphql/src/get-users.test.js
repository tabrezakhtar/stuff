const test = require("ava"); // https://www.npmjs.com/package/ava
const sinon = require("sinon"); // https://www.npmjs.com/package/sinon
const proxyquire = require("proxyquire").noCallThru().noPreserveCache(); // https://www.npmjs.com/package/proxyquire

test.beforeEach((t) => {
    t.context.root = {};

    t.context.getUsers = proxyquire("./get-users", { "node-fetch": sinon.stub() });
});

test("should return an array of users in the JSON response", async (t) => {
    const result = await t.context.getUsers(t.context.root);

    t.true(Array.isArray(result));
    t.true(typeof result[0] === "object");
    t.true(typeof result[0].title === "string");
    t.true(typeof result[0].name === "string");
    t.true(typeof result[0].age === "number");
    t.true(typeof result[0].email === "string");
    t.true(typeof result[0].avatar === "string");
});

// Add at least one more test case below...
