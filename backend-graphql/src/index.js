const express = require("express"); // https://www.npmjs.com/package/express
const { ApolloServer, gql } = require("apollo-server-express"); // https://www.npmjs.com/package/apollo-server-express
const getUsers = require("./get-users");

const app = express();

const typeDefs = gql`
    type User {
        title: String!
        name: String!
        age: Int!
        email: String!
        avatar: String!
    }

    type Query {
        getUsers: [User]
    }
`;
const resolvers = {
    getUsers,
};
const graphql = new ApolloServer({ typeDefs, resolvers });

graphql.applyMiddleware({ app });

app.listen(3000, () => {
    console.log("Server listening at http://localhost:3000/graphql");
});
