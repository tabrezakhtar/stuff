const fetch = require("node-fetch"); // https://www.npmjs.com/package/node-fetch

async function getUsers (root) {
    const result = await fetch("https://randomuser.me/api?results=10&nat=gb"); // https://randomuser.me

    // Process the result of the above fetch to match the expected GraphQL result defined in the
    // type definitions (index.js) for the User type...

    return  [
        {
            title: "ms",
            name: "test user",
            age: 30,
            email: "test.user@email.com",
            avatar: "https://randomuser.me/api/portraits/thumb/men/75.jpg",
        },
    ];
}

module.exports = getUsers;
