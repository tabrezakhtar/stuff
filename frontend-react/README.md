# Frontend React Test

## Description

We need to show a list of users with the following information: 

* user's full name
* user's date of birth
* user's avatar
* user's address infomration

## Acceptance Criteria

1. Display full name in the format <first name> <last name>
2. Display date of birth in the format day, month, year - for example 09/01/2021
3. Display user avatar
4. Display address in the format:

Number,
Street, 
Town,
County,
Post code

## Working with the project

| command              | description                                                                          |
| -------------------- | ------------------------------------------------------------------------------------ |
| `npm start`          | Starts a dev server at [http://localhost:3000](http://localhost:3000)                |
| `npm test`           | Runs the test in watch mode                                                          |