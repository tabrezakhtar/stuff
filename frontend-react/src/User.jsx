import PropTypes from "prop-types";

import "./User.css";

function User({ user }) {
    return (
        <div className="user">
            <div className="user-info">
                <div>Name: <span data-testid="username">{user.name.first} {user.name.last}</span></div>
                {/* date of birth */}
            </div>
            <img src={user.picture.medium} alt="avatar" className="user-avatar" data-testid="user-avatar"/>
            <div>
                {/* address */}
            </div>
        </div>
    );
}

User.propTypes = {
    user: PropTypes.shape({
        name: PropTypes.shape({
            first: PropTypes.string.isRequired,
            last: PropTypes.string.isRequired,
        }).isRequired,
        picture: PropTypes.shape({
            medium: PropTypes.string.isRequired
        }).isRequired
    }).isRequired
}

export default User;
