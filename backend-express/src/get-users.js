const fetch = require("node-fetch"); // https://www.npmjs.com/package/node-fetch

async function getUsers (req, res) {
    try {
        const result = await fetch("https://randomuser.me/api?results=10&nat=gb"); // https://randomuser.me

        if (result.ok) {
            const { results } = await result.json();

            return res.json({
                status: result.status,
                users: results.map(u => ({
                    title: u.name.title,
                    name: `${u.name.first} ${u.name.last}`,
                    age: u.dob.age,
                    email: u.email,
                    avatar: u.picture.thumbnail
                }))
            });
        }

        throw new Error("oops");
    } catch(error) {
        return res.json({
            status: 500,
            error: error.message
        });
    }
}

module.exports = getUsers;
