# Backend Express Test

## Instructions

Read the [description](#description) of the task below and complete the
[acceptance criteria](#acceptance-criteria). Feel free to change the structure of the project, as
well as add/remove any dependencies, as you see fit.

Files are located in the `/src` directory. NPM Commands can be found in the
[working with the project](#working-with-the-project) section.

## Description

We've had a request for a new endpoint on our Express API server. Our front-end application needs an
`/api/users` endpoint that returns an array of users from an external system
([https://randomuser.me](https://randomuser.me)).

We've already hard-coded an example response so the front-end devs can crack on with their side of
things, but now we need to do the implementation for real.

## Acceptance Criteria

1. There must be an `/api/users` endpoint that returns an array of users from [https://randomuser.me](https://randomuser.me).
2. The JSON response from the endpoint must have the following shape (as an example):
   
```json
{
    "status": 200,
    "users": [
        {
            ...
        }
    ]
}
```

3. A single user in the JSON response must have the following shape (as an example):

```json
{
    "title": "ms",
    "name": "test user",
    "age:" 30,
    "email": "test.user@email.com",
    "avatar": "https://randomuser.me/api/portraits/thumb/men/75.jpg"
}
```

4. If an error occurs on the server when processing a request to the endpoint, the JSON response must have the following shape (as an example):
   
```json
{
    "status": 500,
    "error:": "Internal server error"
}
```

5. We would like to keep the endpoint well tested, so there needs to be at least 2 test cases written in the unit test suite.

## Working with the project

There are a few commands that have already been added to the project:

| command              | description                                                                          |
| -------------------- | ------------------------------------------------------------------------------------ |
| `npm start`          | Starts the express server at [http://localhost:3000](http://localhost:3000).         |
| `npm run dev`        | Starts the server with `Nodemon` (live reloading).                                   |
| `npm run test`       | Runs the unit test suite and prints the code coverage report.                        |
| `npm run test:watch` | Runs the test suite in watch mode (watches for files changes and re-runs the tests). |
